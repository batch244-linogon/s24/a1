// NUMBER 1 - 8

// Cube

	const getCube = 2 ** 3;
	console.log(`The cube of 2 is ${getCube}`);

// Address
	
	const address = ["258","Washington Ave NW","California","90011"];
	const [blockNumber , Street , State, zipCode ] = address;
	console.log(`I live at ${blockNumber} ${Street} ${State} ${zipCode}`);

// Animal
	

	const animal = {
			name: "Lolong",
			type: "salwater crocodile.",
			weight: "1075",
			measurement: "20 ft. 3 in."
		};

	const { name, type, weight, measurement  } = animal;

	console.log(`${name} was a ${type} He weighed at ${weight} kgs with a measurement of ${measurement}`);

	// 9. Create an array of numbers.
const numbers = [1,2,3,4,5]
// 10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
numbers.forEach((number) =>{
	console.log(`${number}`)
});
// 11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.

let reduceNumber = numbers.reduce(function(x,y){
	const add = (x, y) => x + y;
	return x + y;
	})
console.log(reduceNumber);
// 12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
};
// 13. Create/instantiate a new object from the class Dog and console log the object.
let myDog = new Dog()

	myDog.name = "Frankie";
	myDog.age = 5;
	myDog.breed = "Miniature Dachshund"; 
	console.log(myDog);